const config = {
  title: "DD-People",
  subtitle:
    "The distributed database of People (DD-People) is an ownerless and trustless registry of individuals. It uses the Open Index Protocol spec to record data in the immutable transactions of the FLO blockchain.",
  ipfs: {
    apiUrl: "https://ipfs.io/ipfs/"
  },
  floExplorer: {
    url: "https://livenet.flocha.in/"
  },
  frontPage: {
    numberOfNewestRecordsToShow: 12,
    aboutTitle: "DD-People is more than just a registry",
    aboutShortText:
      "We believe you should own your data. We created an ecosystem of distributed databases (DDX).",
    aboutImage: "one.png"
  },
  oip: {
    perPage: 12,
    daemonApiUrl: "https://api.oip.io/oip/o5/",
    baseTemplate: "tmpl_B6E9AF9B",
    requiredTemplates: [],
    addressesWhiteList: [
      "FEQXuxEgfGoEnZbPCHPfHQDMUqXq6tpw4X",
      "FB41shJxHWphSMp2kG9atqSZ4yWVqUWsaH"
    ] //"F6R95XtThjfDr2uGgPPAyG3QS2749osmdC"]
  },
  cardInfo: {
    name: {
      tmpl: "tmpl_20AD45E7",
      name: "name"
    },
    surname: {
      tmpl: "tmpl_B6E9AF9B",
      name: "surname"
    },
    placeOfBirth: {
      tmpl: "tmpl_B6E9AF9B",
      name: "placeOfBirth"
    },
    description: {
      tmpl: "tmpl_20AD45E7",
      name: "description"
    },
    avatarRecord: {
      tmpl: "tmpl_20AD45E7",
      name: "avatar"
    }
  },
  imageHandler: {
    thumbnail: {
      tmpl: "tmpl_1AC73C98",
      name: "thumbnailAddress"
    },
    main: {
      tmpl: "tmpl_1AC73C98",
      name: "imageAddress"
    }
  }
};

export { config };
