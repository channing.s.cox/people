# [DD-Sample Dashboard]

DD-Sample was created from ddx-react that was created using [create-react-app](https://github.com/facebook/create-react-app) and it uses the [Material UI](https://github.com/mui-org/material-ui) framework inspired by Google's Material Design.

## Quick start

Quick start options:

- `npm install`
- `npm run build`
- `npm run start`

## Licensing

- Licensed under MIT
